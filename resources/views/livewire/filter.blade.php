<div>
    <div class="container">
	    <div class="row">
	        <div class="col-md-12">	            
	            <input type="text"  class="form-control" placeholder="Search" wire:model="searchTerm" />
	            <table class="table table-bordered" style="margin: 10px 0 10px 0;">
	                <tr>
	                    <th>Name</th>
	                    <th>Email</th>
	                    <th>Actions</th>
	                </tr>
	                @foreach($exams as $user)
	                <tr>
	                    <td>
	                        {{ $user->question }}
	                    </td>
	                    <td>
	                        {{ $user->ans }}
	                    </td>
                        <td>EDIT|DELETE</td>
	                </tr>
	                @endforeach
	            </table>
	            {{ $exams->links('pagination::bootstrap-4') }}
                
	        </div>
	    </div>
	</div>
</div>
