<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                <h3>Question Form</h3>
                @if(Session::has("error_msg"))
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top:10px;">
                {{ Session::get("error_msg")}}
                  <button type="button" class="close" data-dismiss="alert" arial-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>                  
              @endif
              @if(Session::has("success_msg"))
              <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top:10px;">
                    {{ Session::get("success_msg")}}
                  <button type="button" class="close" data-dismiss="alert" arial-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>  
              @endif         
              @if($errors->any())
            <div class="alert-danger py-4">
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
          @endif   
          

                <form class="col-6" method="post" action="{{ route('exam.store',$record->id) }}">
                    @csrf
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Question</label>
                    <textarea class="form-control" name="question" id="exampleFormControlTextarea1" rows="3" required>{{$record->question}}</textarea>
                </div>                    
                <div class="form-group">
                    <label for="exampleFormControlInput1">Option 1</label>
                    <input type="text" name="opt1" class="form-control" id="exampleFormControlInput1" value="{{$record->op1}}" placeholder="option 1" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Option 2</label>
                    <input type="text" name="opt2" class="form-control" id="exampleFormControlInput1" value="{{$record->op2}}" placeholder="option 2" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Option 3</label>
                    <input type="text" name="opt3" class="form-control" id="exampleFormControlInput1" value="{{$record->op3}}" placeholder="option 3" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Option 4</label>
                    <input type="text" name="opt4" class="form-control" id="exampleFormControlInput1" value="{{$record->op4}}" placeholder="option 4" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Answare</label>
                    <input type="text" name="ans" class="form-control" id="exampleFormControlInput1" value="{{$record->ans}}" placeholder="answare" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Type Question</label>
                    <select class="form-control" name="type" id="exampleFormControlSelect1">
                    <option value="">Select Type</option>
                    <option value="technical" @if($record->type == "technical") selected @endif>technical</option>
                    <option value="aptitude" @if($record->type == "aptitude") selected @endif>aptitude</option>
                    <option value="logical"  @if($record->type == "logical") selected @endif>logical</option>               
                    </select>
                </div>
                
                <div class="form-group">
                   <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
                </form>


                </div>
            </div>
        </div>
    </div>
</x-app-layout>
