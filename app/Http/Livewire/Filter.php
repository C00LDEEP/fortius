<?php

namespace App\Http\Livewire;

use Livewire\Component;

use Livewire\WithPagination;
use Illuminate\Pagination\Paginator;
use App\Models\Exam;
class Filter extends Component
{
    use WithPagination;
    public $searchTerm;

    public function render()
    {
    	$query = '%'.$this->searchTerm.'%';

    	return view('livewire.filter', [
    		'exams'		=>	Exam::where(function($sub_query){
    							$sub_query->where('question', 'like', '%'.$this->searchTerm.'%');
    									  //->orWhere('email', 'like', '%'.$this->searchTerm.'%');
    						})->paginate(10)
    	]);
    }

    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }
}
