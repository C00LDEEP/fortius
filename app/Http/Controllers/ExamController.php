<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exam;
use Session;
class ExamController extends Controller
{
    public function index(){
        return view("exam.create");
    }
    
    public function store(Request $request,$id=null){
        $data = $request->all();
        $rule = [
            "question" => "required|unique:exams",
            "opt1" => "required",
            "opt2" => "required",
            "opt3" => "required",
            "opt4" => "required",
            "ans" => "required",
            "type" => "required",
        ];
        $ruleMessage =[
            "question.required" => "Please enter queastion",
            "question.unique" => "Please enter unique question",
        ];
        $this->validate($request,$rule,$ruleMessage);

        if($request->isMethod("POST")){
            if($id == null){
                Exam::create(["question" => $data["question"],"op1" => $data["opt1"],"op2" => $data["opt2"],"op3" => $data["opt3"],"op4" => $data["opt4"] ,"ans" => $data["ans"],"type"=>$data["type"] ]);
                Session::flash("success_msg","Succesfully added to table");   
            }
            else{
                Exam::where('id',$id)->update(["question" => $data["question"],"op1" => $data["opt1"],"op2" => $data["opt2"],"op3" => $data["opt3"],"op4" => $data["opt4"] ,"ans" => $data["ans"],"type"=>$data["type"] ]);
                Session::flash("success_msg","Succesfully updated the record"); 
            }         
            return redirect()->route("dashboard");            
        }
    }

    public function edit($id){
        $record = Exam::find($id);
        return view("exam.edit",compact("record"));
    }


    public function destroy(Request $request){
        $data = $request->all();
        if(!empty($data["id"])){
            $record = Exam::find($data["id"]);
            $record->delete();
            //Session::flash("success_msg","You have succesfully deleted the Exam record."); 
        }       
        return response()->json(["success" => 1]);            
    }

    public function getEmployees(Request $request){

        ## Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = Exam::select('count(*) as exams')->count();
        $totalRecordswithFilter = Exam::select('count(*) as allcount')->where('question', 'like', '%' .$searchValue . '%')->count();

        // Fetch records
        $records = Exam::orderBy($columnName,$columnSortOrder)
               ->where('exams.question', 'like', '%' .$searchValue . '%')
              ->select('exams.*')
              ->skip($start)
              ->take($rowperpage)
              ->get();

        $data_arr = array();

        foreach($records as $record){
           $id = $record->id;
           $question = $record->question;
           $ans = $record->ans;           

           $data_arr[] = array(
               "id" => $id,
               "question" => $question,
               "ans" => $ans            
           );
        }

        $response = array(
           "draw" => intval($draw),
           "iTotalRecords" => $totalRecords,
           "iTotalDisplayRecords" => $totalRecordswithFilter,
           "aaData" => $data_arr
        );

        return response()->json($response); 
     
    }
}
