<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;

 
    protected $fillable = [
        'question',
        'op1',
        'op2',
        'op3',
        'op4',
        'ans',
        'type',
    ];    
}
