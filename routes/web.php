<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ExamController;
use App\Models\Exam;
Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
//    $questions = Exam::all()->groupby("type");
  //  $questions = json_decode(json_encode($questions),true);
    
//    return view('dashboard',compact('questions'));
    return view('dashboard');

})->middleware(['auth'])->name('dashboard');

Route::get("exam",[ExamController::class,"index"])->name("exam.index");
Route::post("exam/store/{id?}",[ExamController::class,"store"])->name("exam.store");
Route::get("exam/edit/{exam}",[ExamController::class,"edit"])->name("exam.edit");
// Route::post("exam/edit/{id}",[ExamController::class,"edit"])->name("exam.edit");
Route::post("exam/destroy",[ExamController::class,"destroy"])->name("exam.destroy");

Route::get('/', [ExamController::class, 'index']); 
Route::get('/getEmployees', [ExamController::class, 'getEmployees'])->name('getEmployees');
Route::get('/livewire/message/filter', [ExamController::class, 'getEmployees']);

require __DIR__.'/auth.php';
