$(document).ready(function () {
    $(".confirmDelete").click(function () {
        let id = $(this).attr("record");
        if (confirm("are you sure? you want to delete it")) {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": jQuery('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
            });
            $.ajax({
                type: "post",
                url: "/exam/destroy",
                data: { id: id },
                success: function (data) {
                    console.log(data);
                    if (data.success) {
                        alert("#row-" + id);
                        $("#row-" + id).html("");
                    }
                },
                error: function (e) {
                    console.log(e);
                },
            });
        }
    });
});
