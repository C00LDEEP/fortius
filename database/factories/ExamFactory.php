<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ExamFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => "technical",
            'question' => $this->faker->lexify($string = '????'),
            'op1' => "opt1",
            'op2' => "opt2",
            'op3' => "opt3",
            'op4' => "opt4",
            "ans"=>$this->faker->randomElement(["opt1","opt2","opt3","opt4"]),        
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
